package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.twuc.bagSaving.CabinetFactory.*;
import static org.junit.jupiter.api.Assertions.*;

class SaveAndGetBagTest extends BagSavingArgument {
    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneEmptyLockerAndSavableSizes")
    void should_get_a_ticket_when_saving_a_bag(Cabinet cabinet, BagSize bagSize, LockerSize lockerSize) {
        Ticket ticket = cabinet.save(new Bag(bagSize), lockerSize);
        assertNotNull(ticket);
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneEmptyLockerAndSavableSizes")
    void should_save_and_get_bag_when_locker_is_empty(Cabinet cabinet, BagSize bagSize, LockerSize lockerSize) {
        Bag savedBag = new Bag(bagSize);
        Ticket ticket = cabinet.save(savedBag, lockerSize);
        Bag fetchedBag = cabinet.getBag(ticket);
        assertSame(savedBag, fetchedBag);
    }

    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.BagSavingArgument#createSavableBagSizeAndLockerSize"})
    void should_save_smaller_bag_to_bigger_locker(
            BagSize smallerBagSize, LockerSize biggerLockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();

        Bag smallerBag = new Bag(smallerBagSize);
        Ticket ticket = cabinet.save(smallerBag, biggerLockerSize);
        Bag receivedBag = cabinet.getBag(ticket);

        assertSame(smallerBag, receivedBag);
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createNonSavableBagSizeAndLockerSize")
    void should_throw_when_saving_bigger_bag_to_smaller_locker(BagSize biggerBagSize, LockerSize smallerLockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class,
                        () -> cabinet.save(new Bag(biggerBagSize), smallerLockerSize));

        assertEquals(
                String.format("Cannot save %s bag to %s locker.", biggerBagSize, smallerLockerSize),
                exception.getMessage());
    }

    @Test
    void should_throw_if_locker_size_is_not_specified() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        assertThrows(
                IllegalArgumentException.class,
                () -> cabinet.save(new Bag(BagSize.BIG), null));
    }

    @Test
    void should_throw_if_no_ticket_is_provided() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();

        final IllegalArgumentException error = assertThrows(
                IllegalArgumentException.class,
                () -> cabinet.getBag(null));
        assertEquals("Please use your ticket.", error.getMessage());
    }

    @Test
    void should_throw_if_ticket_is_not_provided_by_cabinet() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Ticket ticket = new Ticket();

        final IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> cabinet.getBag(ticket));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createSavableBagSizeAndLockerSize")
    void should_throw_if_ticket_is_used(BagSize bagSize, LockerSize lockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Bag bag = new Bag(bagSize);
        Ticket ticket = cabinet.save(bag, lockerSize);
        cabinet.getBag(ticket);

        final IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> cabinet.getBag(ticket));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createSavableBagSizeAndLockerSize")
    void should_throw_if_ticket_is_generated_by_another_cabinet(BagSize bagSize, LockerSize lockerSize) {
        Cabinet anotherCabinet = createCabinetWithPlentyOfCapacity();
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();

        Ticket generatedByAnotherCabinet = anotherCabinet.save(new Bag(bagSize), lockerSize);

        final IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> cabinet.getBag(generatedByAnotherCabinet));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneLockerSizeFull")
    void should_throw_if_correspond_lockers_are_full(
            Cabinet fullCabinet, BagSize bagSize, LockerSize lockerSize) {
        Bag savedBag = new Bag(bagSize);
        InsufficientLockersException error = assertThrows(
                InsufficientLockersException.class,
                () -> fullCabinet.save(savedBag, lockerSize));

        assertEquals("Insufficient empty lockers.", error.getMessage());
    }

    @ParameterizedTest
    @EnumSource(value = LockerSize.class, mode = EnumSource.Mode.EXCLUDE)
    void should_throw_when_saving_nothing(LockerSize lockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        final IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> cabinet.save(null, lockerSize));

        assertEquals("Please at least put something here.", exception.getMessage());
    }

    @ParameterizedTest
    @EnumSource(value = BagSize.class, names = {"HUGE"}, mode = EnumSource.Mode.EXCLUDE)
    void should_get_a_ticket_when_saving_small_bag_with_stupid_assistant(BagSize bagSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Bag bag = new Bag(bagSize);
        StupidAssistant stupidAssistant = new StupidAssistant(cabinet);
        Ticket ticket = stupidAssistant.save(bag);
        assertNotNull(ticket);
    }

    @Test
    void should_get_message_when_I_saving_huge_bag_with_assistant() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Bag bag = new Bag(BagSize.HUGE);
        StupidAssistant stupidAssistant = new StupidAssistant(cabinet);
        final IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> stupidAssistant.save(bag));
        assertEquals("It's to big, I can not save it.", exception.getMessage());
    }

    @Test
    void should_get_my_bag_when_I_giving_ticket_to_assistant() {
        Bag savedBag = new Bag(BagSize.SMALL);
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = new StupidAssistant(cabinet);
        Ticket ticket = stupidAssistant.save(savedBag);
        Bag fetchedBag = stupidAssistant.getBag(ticket);

        assertSame(fetchedBag, savedBag);
    }

    @Test
    void should_get_error_message_when_I_giving_bag_to_assistant_and_the_locker_is_full() {
        Bag bag = new Bag(BagSize.SMALL);
        Cabinet cabinet = createCabinetWithFullLockers(new LockerSize[]{LockerSize.SMALL}, 1);
        StupidAssistant stupidAssistant = new StupidAssistant(cabinet);
        InsufficientLockersException error = assertThrows(
                InsufficientLockersException.class,
                () -> stupidAssistant.save(bag));
        assertEquals("Insufficient empty lockers.", error.getMessage());
    }

    @Test
    void should_throw_when_I_get_bag_without_ticket() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = new StupidAssistant(cabinet);
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> {
            stupidAssistant.getBag(null);
        });
        assertEquals("Please use your ticket.",illegalArgumentException.getMessage());
    }

    @Test
    void should_get_ticket_when_used_assistant_have_2_cabinet() {
        List<Cabinet> cabinets = createCabinets();
        Bag bag = new Bag(BagSize.SMALL);
        StupidAssistant stupidAssistant = new StupidAssistant(cabinets);
        Ticket ticket = stupidAssistant.save(bag);
        assertNotNull(ticket);
    }

    @Test
    void should_get_the_1st_cabinet() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Cabinet anotherCabinet = createCabinetWithPlentyOfCapacity();
        List<Cabinet> cabinets = new ArrayList<>(Arrays.asList(cabinet, anotherCabinet));
        Bag bag = new Bag(BagSize.SMALL);
        StupidAssistant stupidAssistant = new StupidAssistant(cabinets);
        Ticket ticket = stupidAssistant.save(bag);
        Cabinet fetchedCabinet = ticket.getCabinet();
        assertEquals(cabinet, fetchedCabinet);
    }

    @Test
    void should_throw_when_I_use_Invalid_ticket_with_assistant() {
        Bag savedBag = new Bag(BagSize.SMALL);
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = new StupidAssistant(cabinet);
        Ticket ticket = stupidAssistant.save(savedBag);
        stupidAssistant.getBag(ticket);
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> {
            stupidAssistant.getBag(ticket);
        });
        assertEquals("Invalid ticket.", illegalArgumentException.getMessage());
    }

    @Test
    void should_get_ticket_belongs_the_2nd_cabinet() {
        Cabinet cabinet = createCabinetWithFullLockers(new LockerSize[]{LockerSize.SMALL}, 1);
        Cabinet anotherCabinet = createCabinetWithPlentyOfCapacity();
        List<Cabinet> cabinets = new ArrayList<>(Arrays.asList(cabinet, anotherCabinet));
        Bag bag = new Bag(BagSize.SMALL);
        StupidAssistant stupidAssistant = new StupidAssistant(cabinets);
        Ticket ticket = stupidAssistant.save(bag);
        Cabinet fetchedCabinet = ticket.getCabinet();
        assertEquals(anotherCabinet, fetchedCabinet);
    }

    @Test
    void should_throw_when_all_cabinet_is_full() {
        Cabinet cabinet = createCabinetWithFullLockers(new LockerSize[]{LockerSize.SMALL}, 1);
        Cabinet anotherCabinet = createCabinetWithFullLockers(new LockerSize[]{LockerSize.SMALL}, 1);
        List<Cabinet> cabinets = new ArrayList<>(Arrays.asList(cabinet, anotherCabinet));
        Bag bag = new Bag(BagSize.SMALL);
        StupidAssistant stupidAssistant = new StupidAssistant(cabinets);
        InsufficientLockersException insufficientLockersException = assertThrows(InsufficientLockersException.class, () -> {
            stupidAssistant.save(bag);
        });
        assertEquals("Insufficient empty lockers.", insufficientLockersException.getMessage());
    }

    @Test
    void should_get_bag_when_I_use_right_ticket() {
        List<Cabinet> cabinets = createCabinets();
        Bag bag = new Bag(BagSize.SMALL);
        StupidAssistant stupidAssistant = new StupidAssistant(cabinets);
        Ticket ticket = stupidAssistant.save(bag);
        Bag fetchedBag = stupidAssistant.getBag(ticket);
        assertSame(bag, fetchedBag);
    }

    @Test
    void should_throw_when_I_use_the_invalid_ticket() {
        List<Cabinet> cabinets = createCabinets();
        StupidAssistant stupidAssistant = new StupidAssistant(cabinets);
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> {
            stupidAssistant.getBag(new Ticket());
        });
        assertEquals("Invalid ticket.", illegalArgumentException.getMessage());
    }

    @Test
    void should_throw_when_I_get_bag_without_ticket_to_assistant() {
        List<Cabinet> cabinets = createCabinets();
        StupidAssistant stupidAssistant = new StupidAssistant(cabinets);
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> {
            stupidAssistant.getBag(null);
        });
        assertEquals("Please use your ticket.", illegalArgumentException.getMessage());
    }


}
