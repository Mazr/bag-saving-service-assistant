package com.twuc.bagSaving;


import java.util.*;

public class StupidAssistant {
    private List<Cabinet> cabinets;
    private List<Ticket> tickets = new ArrayList<>();

    public StupidAssistant(Cabinet cabinet) {
        this(Collections.singletonList(cabinet));
    }

    public StupidAssistant(List<Cabinet> cabinets) {
        this.cabinets = cabinets;
    }

    public Ticket save(Bag bag) {
        BagSize bagSize = bag.getBagSize();
        int lastCabinet = cabinets.size() - 1;
        int currentCabinet = 0;
        Ticket ticket = new Ticket();
        for (Cabinet cabinet : cabinets) {
            if (currentCabinet == lastCabinet) {
                return autoSaveBag(bag, bagSize, cabinet);
            }
            try {
                return autoSaveBag(bag, bagSize, cabinet);
            } catch (Exception ignored) {
            }
            currentCabinet++;
        }
        return ticket;
    }

    private Ticket autoSaveBag(Bag bag, BagSize bagSize, Cabinet cabinet) {
        switch (bagSize) {
            case BIG:
                return getTicket(bag, cabinet, LockerSize.BIG);
            case MINI:
            case SMALL:
                return getTicket(bag, cabinet, LockerSize.SMALL);
            case MEDIUM:
                return getTicket(bag, cabinet, LockerSize.MEDIUM);
            case HUGE:
                throw new IllegalArgumentException("It's to big, I can not save it.");
            default:
                throw new IllegalStateException("Unexpected value: " + bagSize);
        }
    }

    private Ticket getTicket(Bag bag, Cabinet cabinet, LockerSize lockerSize) {
        Ticket ticket = cabinet.save(bag, lockerSize);
        ticket.setCabinet(cabinet);
        tickets.add(ticket);
        return ticket;
    }

    public Bag getBag(Ticket ticket) {
        if (ticket == null){
            throw new IllegalArgumentException("Please use your ticket.");
        }
        if (!tickets.contains(ticket)){
            throw new IllegalArgumentException("Invalid ticket.");
        }
        tickets.remove(ticket);
        return ticket.getCabinet().getBag(ticket);
    }
}
