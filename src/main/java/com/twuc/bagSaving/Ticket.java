package com.twuc.bagSaving;

public class Ticket {
    private Cabinet cabinet;

    public Ticket() {
    }

    public Cabinet getCabinet() {
        return cabinet;
    }

    public void setCabinet(Cabinet cabinet) {
        this.cabinet = cabinet;
    }

}
